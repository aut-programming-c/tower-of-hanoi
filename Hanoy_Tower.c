#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <windows.h>


int top_disk[4];
int speed=1;

void print_disk(int num,int x,int y,char ch){
    gotoxy(x-num+1,y);
    print_line_ch(ch,num*2-1);
    if(ch==' '  && y>=(19-10)){
        gotoxy(x,y);
        printf("|");
    }
}
void gotoxy( short x, short y ){
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE) ;
    COORD position = { x, y } ;
    SetConsoleCursorPosition( hStdout, position ) ;
}
void print_line_ch(char ch,int leng){
    int i;
    for(i=1 ; i<=leng ;i++){
        printf("%c",ch);
    }
}
void print_body(int num){
    int i,j;
    print_line_ch(177,85);
    puts("");
    for(i=1 ; i<=8 ; i++){
        printf("%c",177);
        print_line_ch(' ',83);
        printf("%c",177);
        puts("");
    }
    for(i=0 ; i<10 ; i++){
        printf("%c",177);
        print_line_ch(' ',20);
        printf("%c",124);
        print_line_ch(' ',20);
        printf("%c",124);
        print_line_ch(' ',20);
        printf("%c",124);
        print_line_ch(' ',20);
        printf("%c",177);
        puts("");
    }
    print_line_ch(177,85);
    puts("");
    printf("%c",177);
    print_line_ch(176,19);
    printf("%c%d%c",174,1,175);
    print_line_ch(176,18);
    printf("%c%d%c",174,2,175);
    print_line_ch(176,18);
    printf("%c%d%c",174,3,175);
    print_line_ch(176,19);
    printf("%c",177);
    puts("");
    print_line_ch(177,85);
    puts("");
    puts("");
    print_line_ch(' ',30);
    printf("Created by Alireza Mazochi");
    top_disk[1]=num;
    for(i=1 ; i<=num ; i++){
        print_disk((num+1-i),21,19-i,219);
    }
  //  print_line_ch(177,85);
}
void move_disk(int start,int finish,int num){
    print_disk(num,21*start,19-top_disk[start],' ');
    move_up(21*start,19-top_disk[start],4,num);
    move_horizontal(4,21*start,21*finish,num);
    move_down(21*finish,19-top_disk[finish]-1,4,num);
    print_disk(num,21*finish,19-top_disk[finish]-1,219);
    top_disk[finish]++;
    top_disk[start]--;
}
void move_up(int x,int down,int up,int num){
    int i;
    for(i=down ; i>=up ; i--){
        print_disk(num,x,i,219);
        Sleep(speed);
        print_disk(num,x,i,' ');
    }
}
void move_down(int x,int down,int up,int num){
    int i;
    for(i=up ; i<=down ; i++){
        print_disk(num,x,i,219);
        Sleep(speed);
        print_disk(num,x,i,' ');
    }
}
void move_horizontal(int y,int x1,int x2,int num){
    int i;
    if(x1<x2){
        for(i=x1 ; i<=x2 ; i++){
            print_disk(num,i,y,219);
            Sleep(speed);
            print_disk(num,i,y,' ');
        }
    }
    else{
        for(i=x1 ; i>=x2 ; i--){
            print_disk(num,i,y,219);
            Sleep(speed);
            print_disk(num,i,y,' ');
        }
    }
}
void transfer(int num,int start, int finish,int carry){
    if(num>0){
        transfer(num-1,start,carry,finish);
        move_disk(start,finish,num);
        //printf("move disk %d from %d to %d\n",num,start,finish);
        transfer(num-1,carry,finish,start);
    }
}
int  input(){
    int flag=1;
    int num;
    while(flag){
        printf("Please input number of disk!\n");
        scanf("%d",&num);
        flag=0;
        system("cls");
        if(num==0 || num>10){
            printf("Your input not valid!\n");
            flag=1;
        }
        if(1<=num && num<=3){
            speed=200;
        }
        else if(num<=5){
            speed=70;
        }
        else if(num<=7){
            speed=30;
        }
        else if(num<=10){
            speed=2;
        }


    }
    return num;
}
int end_program(){
    printf("%c",7);
    Sleep(100);
    system("color F4");
    Sleep(100);
    system("color F6");
    Sleep(100);
    system("color F2");
    Sleep(100);
    system("color F5");
    Sleep(3000);
    gotoxy(0,24);
}
int main(){
    int num;
    system("color F5");
    num=input();
    print_body(num);
    transfer(num,1,3,2);
    end_program();
    return 0;
}
